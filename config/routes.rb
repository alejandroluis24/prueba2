Rails.application.routes.draw do
  devise_for :users
  get 'tasks/:id/complete', to: 'tasks#complete', as: 'task_complete'

  resources :tasks, only: %i[index show]

  root 'tasks#index'
end
