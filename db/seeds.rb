Task.delete_all

Task.create(
    title: 'Bailar cueca',
    description: 'Con pañuelo',
    photo: 'http://www.tvn.cl/incoming/cuecajpg-2492995/ALTERNATES/w1024h768/cueca.jpg'
)

Task.create(
    title: 'Comer empanadas',
    description: 'Excepto de Pino',
    photo: 'https://img-global.cpcdn.com/002_recipes/6fc48b9359980d79/751x532cq70/photo.jpg'
)

Task.create(
    title: 'Tomar terremoto',
    description: 'Con fernet y/o granadina',
    photo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Terremotopiojera.jpg/320px-Terremotopiojera.jpg'
)