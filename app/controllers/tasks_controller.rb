class TasksController < ApplicationController
  before_action :authenticate_user!

  def index
    @tasks = Task.all
  end

  def complete
    task = Task.find(params[:id])

    # Task.find_or_create_by(user: current_user, task: task)
    user_task = task.user_tasks.find_by(user: current_user)

    if user_task == nil
      UserTask.create(user: current_user, task: task, completed: true)
    else
      # Existe el user_task

      # if user_task.completed
      #   user_task.completed = false
      # else
      #   user_task.completed = true
      # end
      user_task.completed = !user_task.completed

      user_task.save
    end

    redirect_to tasks_path
  end

  def show
    @task = Task.find(params[:id])
    @user_tasks = UserTask.where(task: @task).order(updated_at: :desc).limit(5)
  end
end
